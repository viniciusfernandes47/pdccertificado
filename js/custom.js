function getImgFromUrl(logo_url, callback) {
    var img = new Image();
    img.src = logo_url;
    img.onload = function () {
        callback(img);
    };
} 
function generatePDF(img){
    var options = {orientation: 'l', unit: 'mm', format: 'a4'};
    var doc = new jsPDF(options);
    var width = doc.internal.pageSize.getWidth();
    var height = doc.internal.pageSize.getHeight();
    doc.addImage(img, 'JPEG', 0, 0, width, height, 'alias', 'NONE');
    doc.save("Certificado-Saint-Gobain-Webinar-Parceiro.pdf");
}
$(document).on("click","#save",function() {
	var logo_url = $('#certificadoImage').attr('src');
	console.log(logo_url);
    getImgFromUrl(logo_url, function (img) {
        generatePDF(img);
    });
});
function printImg(url) {
    var win = window.open('');
    win.document.write('<img src="' + url + '" onload="window.print();window.close()" />');
    win.focus();
}
$(document).on("click","#print",function() {
	var logo_url = $('#certificadoImage').attr('src');
    printImg(logo_url);
});
function forceDownload(url, fileName){
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);
    xhr.responseType = "blob";
    xhr.onload = function(){
        var urlCreator = window.URL || window.webkitURL;
        var imageUrl = urlCreator.createObjectURL(this.response);
        var tag = document.createElement('a');
        tag.href = imageUrl;
        tag.download = fileName;
        document.body.appendChild(tag);
        tag.click();
        document.body.removeChild(tag);
    }
    xhr.send();
}
$(document).on("click", "#saveImage", function() {
    var gh =  $('#certificadoImage').attr('src');
    forceDownload(gh, 'Certificado-Saint-Gobain-Webinar-Parceiro.png');
});


$("#form-cert").submit(function(event){
    event.preventDefault();

	const URLAPI = 'https://api.bannerbear.com/v2';
    const TEMPLATE = 'YJBpekZX8maZ2XPnOA';
    
    var data_curso = "18.08.2020";
    var carga_horaria = "2 horas";
    var logo_certificado = "http://clientes.diwe.com.br/certificadopdc/white_default.png";

    switch ($('#cert-webinar').val()) {
    case 'Webinar Clube Red Brasilit':
        data_curso = "18.08.2020";
        carga_horaria = "2 horas";
        logo_certificado = "http://clientes.diwe.com.br/certificadopdc/clubered.png";
        break;
    case 'Mitos e verdades nos procedimentos de recuperação de estruturas':
        data_curso = "18.08.2020";
        carga_horaria = "2 horas";
        break;
    break;
    default:
        data_curso = "18.08.2020";
        carga_horaria = "2 horas";
    }

    var data = {
        "template": TEMPLATE,
        "modifications": [
            {
            "name": "nome_aluno",
            "text": $('#cert-name').val(),
            },
            {
            "name": "nome_webinar",
            "text": $('#cert-webinar').val(),
            },
            {
            "name": "data_curso",
            "text": data_curso
            },
            {
            "name": "carga_horaria",
            "text": carga_horaria
            },
            {
            "name": "logo_certificado",
            "image_url": logo_certificado
            }
        ],
        "webhook_url": null,
        "metadata": null
    };

    $.ajax({
        url : `${URLAPI}/images`,
        type : 'POST',
        headers: {
            'Authorization':'Bearer ahPB8QfSVxbXbzUbDYcctgtt'
        },
        contentType: "application/json",
        data : JSON.stringify(data),
        dataType: "json",
        beforeSend : function(){
			$("#cert-send").text("Gerando...");
			$("#cert-send").addClass("cert-send-disabled");
			$("#cert-send").attr("disabled", true);
        },
        success: function(response) {
            console.log(response);
            var responseId = response.uid;
            var intervalo = setInterval(function(){ 
                $.ajax({
                    url : `${URLAPI}/images/${responseId}`,
                    type : 'GET',
                    headers: {
                        'Authorization':'Bearer ahPB8QfSVxbXbzUbDYcctgtt'
                    },
                    success : function(response){
                         console.log(response);
                         if(response.status == 'completed') {
							$('#certificadoImage').attr("src", response.image_url_png);
							$("#certificate-inner").fadeOut('slow');
							$("#cert-generate").fadeIn('slow');
                            clearInterval(intervalo);
                            return; 
                         }
                    },
                });
            }, 1000);
        },
   });



	
})

