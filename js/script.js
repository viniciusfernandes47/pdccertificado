function getImgFromUrl(logo_url, callback) {
    var img = new Image();
    img.src = logo_url;
    img.onload = function () {
        callback(img);
    };
} 
function generatePDF(img){
    var options = {orientation: 'l', unit: 'mm', format: 'a4'};
    var doc = new jsPDF(options);
    var width = doc.internal.pageSize.getWidth();
    var height = doc.internal.pageSize.getHeight();
    doc.addImage(img, 'JPEG', 0, 0, width, height, 'alias', 'NONE');
    doc.save("Certificado-Saint-Gobain-Revendedor-Parceiro.pdf");
}
$(document).on("click","#save",function() {
	var logo_url = $('#certificadoImage').attr('src');
	console.log(logo_url);
    getImgFromUrl(logo_url, function (img) {
        generatePDF(img);
    });
});
function printImg(url) {
    var win = window.open('');
    win.document.write('<img src="' + url + '" onload="window.print();window.close()" />');
    win.focus();
}
$(document).on("click","#print",function() {
	var logo_url = $('#certificadoImage').attr('src');
    printImg(logo_url);
});
function forceDownload(url, fileName){
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);
    xhr.responseType = "blob";
    xhr.onload = function(){
        var urlCreator = window.URL || window.webkitURL;
        var imageUrl = urlCreator.createObjectURL(this.response);
        var tag = document.createElement('a');
        tag.href = imageUrl;
        tag.download = fileName;
        document.body.appendChild(tag);
        tag.click();
        document.body.removeChild(tag);
    }
    xhr.send();
}
$(document).on("click", "#saveImage", function() {
    var gh =  $('#certificadoImage').attr('src');
    forceDownload(gh, 'Certificado-Saint-Gobain-Revendedor-Parceiro.png');
});


$(document).ready(function(){ 
    for (var i = 2; i < 61; i++) {
        if(i != 60) {
            $('.rev-date').append($('<option>', {
                value:  i+' anos.',
                text: i+' anos'
            }));
        }else {
            $('.rev-date').append($('<option>', {
                value:  'mais de 60 anos.',
                text: 'Mais de 60 anos'
            }));
        }
        
     }
});

$("#form-cert").submit(function(event){
    event.preventDefault();

	const URLAPI = 'https://api.bannerbear.com/v2';
	const TEMPLATE = '7wpnPQZzK6W5dOgxoX';
    var data = {
        "template": TEMPLATE,
        "modifications": [
            {
            "name": "nome",
            "text": $('#cert-name').val(),
            },
            {
            "name": "data",
            "text": $('.rev-date').val(),
            },
        ],
        "webhook_url": null,
        "metadata": null
    };

    $.ajax({
        url : `${URLAPI}/images`,
        type : 'POST',
        headers: {
            'Authorization':'Bearer QoIKQ5hq0X1FgYZbbwt7Zwtt'
        },
        contentType: "application/json",
        data : JSON.stringify(data),
        dataType: "json",
        beforeSend : function(){
			$("#cert-send").text("Gerando...");
			$("#cert-send").addClass("cert-send-disabled");
			$("#cert-send").attr("disabled", true);
        },
        success: function(response) {
            console.log(response);
            var responseId = response.uid;
            var intervalo = setInterval(function(){ 
                $.ajax({
                    url : `${URLAPI}/images/${responseId}`,
                    type : 'GET',
                    headers: {
                        'Authorization':'Bearer QoIKQ5hq0X1FgYZbbwt7Zwtt'
                    },
                    success : function(response){
                         console.log(response);
                         if(response.status == 'completed') {
							$('#certificadoImage').attr("src", response.image_url_png);
							$("#certificate-inner").fadeOut('slow');
							$("#cert-generate").fadeIn('slow');
                            clearInterval(intervalo);
                            return; 
                         }
                    },
                });
            }, 1000);
        },
   });



	
})

